import React, {Component} from 'react';
import Title from './components/title'
import Form from './components/form'
import Notes from './components/notes'
// import logo from './logo.svg';
import '../src/styles/scss/index.css';

export default class App extends Component {
  constructor(...args){
    super(...args);


      //contexto de la funcion: no sabe que es this
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handleLanguageChange = this.handleLanguageChange.bind(this);
      this.handleHighlightChange = this.handleHighlightChange.bind(this);
      this.handleNoteKeyPress = this.handleNoteKeyPress.bind(this);

    //estado en el que se inicia app
    this.state = {
        name: 'Saray',
        language: 'es',
        highlight: false,
        notes: []

    }
  }

  //hacer que cambie
    handleNameChange(e){
      //permite setear estado
      this.setState({
          name: e.target.value
      })
    }

    handleLanguageChange(e){
        this.setState({
            language: e.target.value
        })
    }

    handleHighlightChange(e){
        this.setState({
            highlight: e.target.checked
        })
    }

    handleNoteKeyPress(e){
        //referencia estado actual
        const notes = this.state.notes.slice();
        if(e.key === 'Enter'){
            notes.push(e.target.value);
            console.log(notes);
           this.setState({
            notes: notes
           })
        }
    }

  render() {
        const {notes} = this.state;
    return(
        <div>
          <Title name={this.state.name}
                 language={this.state.language}
                 highlight={this.state.highlight}
          />
          <Form handleNameChange={this.handleNameChange}
                handleLanguageChange={this.handleLanguageChange}
                handleHighlightChange={this.handleHighlightChange}
                handleNoteKeyPress={this.handleNoteKeyPress}
                highlight = {this.state.highlight}
                name={this.state.name}
                language={this.state.language}
          />

            <div className='sidebar'>
                {/*<h2>Notes</h2>*/}
                <Notes notes={notes}/>
            </div>
        </div>



    )
  }
}

// export default App;
