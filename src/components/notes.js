import React, {Component} from 'react'
import PropTypes from 'prop-types';

export default function Notes({notes}) {
    const hasNotes = notes.length > 0;
    return(
        <div>
            <h2>Notes</h2>
            {/*if*/}
            {hasNotes > 0 &&
            <ul className='notes'>
                {notes.map((note,index)=>{
                    return <li className='notes-item' key={index} value={note}>{note}</li>
                })}

            </ul>
            }
            {/*else*/}
            {!hasNotes && <strong>No notes.</strong>}
        </div>
    );
}

Notes.propTypes = {
    notes: PropTypes.array.isRequired
};