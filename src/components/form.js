import React, {Component} from 'react'
import PropTypes from 'prop-types';
import { languages } from '../config2';

export default function Form ({
                                  name,
                                  handleNameChange,
                                  language,
                                  handleLanguageChange,
                                  highlight,
                                  handleHighlightChange,
                                  handleNoteKeyPress}) {
    return(
        <div>
            <div className='form-field'>
                <label htmlFor='name'>Name: </label>
                <input type='text' id='name'
                       value={name}
                       onChange={handleNameChange}
                />
            </div>
            <div className='form-field'>
                <label htmlFor='language'>Language:</label>
                <select id='language' value={language} onChange={handleLanguageChange}>
                    {/*atrib que estamos iterando e index*/}
                    {languages.map((language,index) =>{
                        return <option value={language} key={index}>{language}</option>
                    })}
                </select>
            </div>

            <div className='form-field'>
                <label htmlFor='highlight'>Highlight:</label>
                <input
                    type='checkbox'
                    id='highlight'
                    checked={highlight}
                    onChange={handleHighlightChange}
                />
            </div>
            <div className='form-field'>
                <label htmlFor='notes'>Add notes:</label>
                <input
                    type='text'
                    id='notes'
                    onKeyPress={handleNoteKeyPress}
                />


            </div>

        </div>
    )

}

Form.propTypes = {
    name: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
    handleNameChange: PropTypes.func.isRequired,
    handleLanguageChange: PropTypes.func.isRequired,
    handleNoteKeyPress: PropTypes.func.isRequired,
};