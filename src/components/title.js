import React,{Component} from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames'
import {translations} from "../data";

export default function Title({name,language,highlight}){
        const title = translations[language].TITLE.replace('%name%', name);
        const className = ClassNames('title',{
           'title--highlighted': highlight
        });
        return <h1 className={className}>{title}</h1>;
    }



Title.PropTypes = {
    name: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
    highlight: PropTypes.bool.isRequired
};