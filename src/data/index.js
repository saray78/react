const translations ={
    'es':{
        TITLE: '¡Hola %name%!'
    },
    'en':{
        TITLE: 'Hello %name%!'
    },
    'fr':{
        TITLE: 'Bonjour %name%!'
    },
    'it':{
        TITLE: 'Ciao %name%!'
    },
};

export {translations};

